# Steps to restore the database

> Assumes you have PostgreSQL16
> If you don't have it, visit the following link
> [Download PostgreSQL](https://www.postgresql.org/download/)

1. Download the backup that's in the main path of the project
2. Restore the database


# Steps to set up this porject

> Assumes you have virtual environment installed
> If you don't have it, visit the following link
> [venv instalation](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#:~:text=To%20create%20a%20virtual%20environment,virtualenv%20in%20the%20below%20commands.&text=The%20second%20argument%20is%20the,project%20and%20call%20it%20env%20.)

1. Clone this repository
2. In project root activate the venv:
   If you have Unix Distribution
   `source venv/bin/activate`
   But, if you have Windows you must be run
   `source venv/Scripts/activate`
   ***All the libraries you need are in the venv.***
3. You must create an *.env* file in *turnero* directory. Should be as ***turnero/.env***.
4. In the *.env* file you must configure these environment variables
  DATABASE_NAME=turnero
  DATABASE_USER=
  DATABASE_PASSWORD=
  DATABASE_HOST=localhost
  DATABASE_PORT=
  SECRET_KEY=django-insecure-pkj49lao!klzgiu)7kx6=6nck#p8hv6%nk6iykum$7n8x(r**+
5. You must run
   `python manage.py makemigrations`
   `python manage.py migrate`
   If you have some Unix distribution change python for python3 or another version you're using.
6. Finally, run the server
   `python manage.py runserver`