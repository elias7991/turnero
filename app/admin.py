from django.contrib import admin
from . import models

class PaisAdmin(admin.ModelAdmin):
  list_display = ["descripcion_pais", "codigo_iso2", "codigo_iso3", "estado_pais", "insercion_pais"]
  list_filter = ["estado_pais"]
  list_editable = ["estado_pais"]
  search_fields = ["descripcion_pais", "codigo_iso2", "codigo_iso3", "estado_pais"]

class SexoAdmin(admin.ModelAdmin):
  list_display = ["descripcion_sexo", "estado_sexo", "insercion_sexo"]
  list_filter = ["estado_sexo"]
  list_editable = ["estado_sexo"]
  search_fields = ["descripcion_sexo", "estado_sexo"]

class PrioridadAdmin(admin.ModelAdmin):
  list_display = ["descripcion_prioridad", "estado_prioridad", "insercion_prioridad"]
  list_filter = ["estado_prioridad"]
  list_editable = ["estado_prioridad"]
  search_filter = ["descripcion_prioridad", "estado_prioridad"]

class TipoClienteAdmin(admin.ModelAdmin):
  list_display = ["codigo_tipo_cliente", "descripcion_tipo_cliente", "estado_tipo_cliente", "insercion_tipo_cliente"]
  list_filter = ["estado_tipo_cliente"]
  list_editable = ["estado_tipo_cliente"]
  search_filter = ["descripcion_tipo_cliente", "estado_tipo_cliente"]

class TipoDocumentoAdmin(admin.ModelAdmin):
  list_display = ["codigo_tipo_documento", "descripcion_tipo_documento", "estado_tipo_documento", "insercion_tipo_documento"]
  list_filter = ["estado_tipo_documento"]
  list_editable = ["estado_tipo_documento"]
  search_filter = ["descripcion_tipo_documento", "estado_tipo_documento"]

class TipoContactoAdmin(admin.ModelAdmin):
  list_display = ["codigo_tipo_contacto", "descripcion_tipo_contacto", "estado_tipo_contacto", "insercion_tipo_contacto"]
  list_filter = ["estado_tipo_contacto"]
  list_editable = ["estado_tipo_contacto"]
  search_fields = ["descripcion_tipo_contacto", "estado_tipo_contacto"]

class DepartamentoAdmin(admin.ModelAdmin):
  list_display = ["codigo_departamento", "descripcion_departamento", "estado_departamento", "insercion_departamento"]
  list_filter = ["estado_departamento"]
  list_editable = ["estado_departamento"]
  search_fields = ["descripcion_departamento", "estado_departamento"]

class CiudadAdmin(admin.ModelAdmin):
  list_display = ["codigo_ciudad", "descripcion_ciudad", "estado_ciudad", "insercion_ciudad"]
  list_filter = ["estado_ciudad"]
  list_editable = ["estado_ciudad"]
  search_fields = ["descripcion_ciudad", "estado_ciudad"]

class BarrioAdmin(admin.ModelAdmin):
  list_display = ["codigo_barrio", "descripcion_barrio", "estado_barrio", "insercion_barrio"]
  list_filter = ["estado_barrio"]
  list_editable = ["estado_barrio"]
  search_fields = ["descripcion_barrio", "estado_barrio"]

class NacionalidadAdmin(admin.ModelAdmin):
  list_display = ["codigo_nacionalidad", "descripcion_nacionalidad", "estado_nacionalidad", "insercion_nacionalidad"]
  list_filter = ["estado_nacionalidad"]
  list_editable = ["estado_nacionalidad"]
  search_fields = ["descripcion_nacionalidad", "estado_nacionalidad"]

class PersonaAdmin(admin.ModelAdmin):
  list_display = ["nombres_persona", "apellidos_persona", "nacimiento_persona", "direccion_persona", "activo_persona", "insercion_persona"]
  list_filter = ["nacimiento_persona", "activo_persona"]
  list_editable = ["activo_persona", "direccion_persona"]
  search_fields = ["nombres_persona", "apellidos_persona", "activo_persona"]

class CategoriaServicioAdmin(admin.ModelAdmin):
  list_display = ["descripcion_categoria_servicio", "estado_categoria_servicio", "insercion_categoria_servicio"]
  list_filter = ["estado_categoria_servicio"]
  list_editable = ["estado_categoria_servicio"]
  search_fields = ["descripcion_categoria_servicio", "estado_categoria_servicio", "insercion_categoria_servicio"]

class ServicioAdmin(admin.ModelAdmin):
  list_display = ["descripcion_servicio", "costo_servicio", "estado_servicio", "insercion_servicio"]
  list_filter = ["costo_servicio", "estado_servicio"]
  list_editable = ["estado_servicio"]
  search_fields = ["descripcion_servicio", "estado_servicio"]

# Register your models here.

# don't depend on another class
admin.site.register(models.Pais, PaisAdmin)
admin.site.register(models.Prioridad, PrioridadAdmin)
admin.site.register(models.Sexo, SexoAdmin)
admin.site.register(models.TipoCliente, TipoClienteAdmin)
admin.site.register(models.TipoDocumento, TipoDocumentoAdmin)
admin.site.register(models.TipoContacto, TipoContactoAdmin)
admin.site.register(models.Departamento, DepartamentoAdmin)
admin.site.register(models.Ciudad, CiudadAdmin)
admin.site.register(models.Barrio, BarrioAdmin)
admin.site.register(models.Nacionalidad, NacionalidadAdmin)
admin.site.register(models.Persona, PersonaAdmin)
admin.site.register(models.CategoriaServicio, CategoriaServicioAdmin)
admin.site.register(models.Servicio, ServicioAdmin)