from django import forms
from .models import Persona, TipoDocumento, Prioridad, Barrio, Sexo, Nacionalidad, Servicio, CategoriaServicio


class AddPersonForm(forms.ModelForm):
  class Meta:
    model = Persona
    fields = '__all__'
    exclude = ['modificacion_persona']
    labels = {
      'id_tipo_documento_fk_persona': 'Tipo Documento',
      # 'id_tipo_contacto_fk_persona': 'Tipo Contacto',
      'id_prioridad_fk_persona': 'Prioridad',
      'id_barrio_fk_persona': 'Barrio',
      'id_sexo_fk_persona': 'Sexo',
      'id_nacionalidad_fk_persona': 'Nacionalidad',
      'nombres_persona': 'Nombre',
      'apellidos_persona': 'Apellido',
      'cedula_persona': 'Cédula de Identidad',
      'nacimiento_persona': 'Fecha de Nacimiento',
      'contacto_persona': 'Contacto Telefónico',
      'correo_persona': 'Correo Electrónico',
      'direccion_persona': 'Dirección',
      'activo_persona': 'Estado: ',
      'insercion_persona': 'Fecha de inserción'
    }
    widgets = {
      'id_tipo_documento_fk_persona': forms.Select(choices=TipoDocumento.objects.all()),
      # 'id_tipo_contacto_fk_persona': forms.Select(choices=TipoContacto.objects.all()),
      'id_prioridad_fk_persona': forms.Select(choices=Prioridad.objects.all()),
      'id_barrio_fk_persona': forms.Select(choices=Barrio.objects.all()),
      'id_sexo_fk_persona': forms.Select(choices=Sexo.objects.all()),
      'id_nacionalidad_fk_persona': forms.Select(choices=Nacionalidad.objects.all()),
      'nombres_persona': forms.TextInput(
        attrs={
          'class':'form-control',
          'placeholder': 'Ingrese el nombre de la persona'
        }
      ),
      'apellidos_persona': forms.TextInput(
        attrs={
          'class':'form-control',
          'placeholder': 'Ingrese el apellido de la persona'
        }
      ),
      'cedula_persona': forms.TextInput(
        attrs={
          'class':'form-control',
          'placeholder': 'Ingrese la cédula de la persona'
        }
      ),
      'nacimiento_persona': forms.DateInput(
        attrs={
          'class':'form-control',
          'placeholder': 'MM/DD/YYYY'
        },
        format="MM/DD/YYYY"
      ),
      'contacto_persona': forms.TextInput(
        attrs={
          'class':'form-control',
          'placeholder': '09XXXXXXXX'
        }
      ),
      'correo_persona': forms.TextInput(
        attrs={
          'class':'form-control',
          'placeholder': 'example@example.com'
        }
      ),
      'direccion_persona': forms.TextInput(
        attrs={
          'class':'form-control',
          'placeholder': 'Ingrese la dirección de la persona'
        }
      ),
      'insercion_persona': forms.DateTimeInput(
        format=('MM/DD/YYYY'),
        attrs={
          'class': 'form-control',
          'placeholder': 'Seleccione la fecha de inserción.',
          'type': 'date'
        }
      )
    }

    def __init__(self, *args, **kwargs):
      super().__init__(*args, **kwargs)

class AddServiceForm(forms.ModelForm):
  class Meta:
    model = Servicio
    fields = '__all__'
    exclude = ['modificacion_servicio', 'codigo_servicio']
    labels = {
      'id_categoria_servicio_fk_servicio': 'Categoría Servicio',
      'descripcion_servicio': 'Descripción Servicio',
      'costo_servicio': 'Costo Servicio',
      'estado_servicio': 'Estado: ',
      'insercion_servicio': 'Fecha de Inserción'
    }
    widgets = {
      'id_categoria_servicio_fk_servicio': forms.Select(choices=CategoriaServicio.objects.all()),
      'descripcion_servicio': forms.TextInput(
        attrs={
          'class':'form-control',
          'placeholder': 'Ingrese la descripción del servicio a proveer'
        }
      ),
      'costo_servicio': forms.NumberInput(
        attrs={
          'class': 'form-control',
          'placeholder': 'Ingrese el costo del servicio a proveer'
        }
      ),
      'insercion_servicio': forms.DateTimeInput(
        format=('MM/DD/YYYY'),
        attrs={
          'class': 'form-control',
          'placeholder': 'Seleccione la fecha de inserción.',
          'type': 'date'
        }
      )
    }