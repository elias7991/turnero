# Generated by Django 4.1.7 on 2023-02-17 02:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AuthGroup',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, unique=True)),
            ],
            options={
                'db_table': 'auth_group',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='AuthGroupPermissions',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
            ],
            options={
                'db_table': 'auth_group_permissions',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='AuthPermission',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('codename', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'auth_permission',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='AuthUser',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128)),
                ('last_login', models.DateTimeField(blank=True, null=True)),
                ('is_superuser', models.BooleanField()),
                ('username', models.CharField(max_length=150, unique=True)),
                ('first_name', models.CharField(max_length=150)),
                ('last_name', models.CharField(max_length=150)),
                ('email', models.CharField(max_length=254)),
                ('is_staff', models.BooleanField()),
                ('is_active', models.BooleanField()),
                ('date_joined', models.DateTimeField()),
            ],
            options={
                'db_table': 'auth_user',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='AuthUserGroups',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
            ],
            options={
                'db_table': 'auth_user_groups',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='AuthUserUserPermissions',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
            ],
            options={
                'db_table': 'auth_user_user_permissions',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DjangoAdminLog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action_time', models.DateTimeField()),
                ('object_id', models.TextField(blank=True, null=True)),
                ('object_repr', models.CharField(max_length=200)),
                ('action_flag', models.SmallIntegerField()),
                ('change_message', models.TextField()),
            ],
            options={
                'db_table': 'django_admin_log',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DjangoContentType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app_label', models.CharField(max_length=100)),
                ('model', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'django_content_type',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DjangoMigrations',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('app', models.CharField(max_length=255)),
                ('name', models.CharField(max_length=255)),
                ('applied', models.DateTimeField()),
            ],
            options={
                'db_table': 'django_migrations',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DjangoSession',
            fields=[
                ('session_key', models.CharField(max_length=40, primary_key=True, serialize=False)),
                ('session_data', models.TextField()),
                ('expire_date', models.DateTimeField()),
            ],
            options={
                'db_table': 'django_session',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Barrio',
            fields=[
                ('id_barrio', models.AutoField(primary_key=True, serialize=False)),
                ('codigo_barrio', models.SmallIntegerField(unique=True)),
                ('descripcion_barrio', models.CharField(max_length=30)),
                ('estado_barrio', models.BooleanField()),
                ('insercion_barrio', models.DateTimeField()),
                ('modificacion_barrio', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'barrio',
            },
        ),
        migrations.CreateModel(
            name='CategoriaServicio',
            fields=[
                ('id_categoria_servicio', models.AutoField(primary_key=True, serialize=False)),
                ('descripcion_categoria_servicio', models.CharField(max_length=32)),
                ('estado_categoria_servicio', models.BooleanField()),
                ('insercion_categoria_servicio', models.DateTimeField()),
                ('modificacion_categoria_servicio', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'categoria_servicio',
            },
        ),
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id_cliente', models.BigAutoField(primary_key=True, serialize=False)),
                ('codigo_cliente', models.BigIntegerField(unique=True)),
                ('razon_social', models.CharField(max_length=128)),
            ],
            options={
                'db_table': 'cliente',
            },
        ),
        migrations.CreateModel(
            name='DetallesEstadoServicio',
            fields=[
                ('id_detalles_estado_servicio', models.BigAutoField(primary_key=True, serialize=False)),
                ('turnos_diarios', models.IntegerField()),
                ('turnos_restantes', models.IntegerField()),
                ('estimacion_tiempo_turno', models.IntegerField()),
            ],
            options={
                'db_table': 'detalles_estado_servicio',
            },
        ),
        migrations.CreateModel(
            name='Horario',
            fields=[
                ('id_horario', models.AutoField(primary_key=True, serialize=False)),
                ('inicio_horario', models.DateTimeField()),
                ('fin_horario', models.DateTimeField()),
                ('insercion_horario', models.DateTimeField()),
                ('modificacion_horario', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'horario',
            },
        ),
        migrations.CreateModel(
            name='Nacionalidad',
            fields=[
                ('id_nacionalidad', models.SmallAutoField(primary_key=True, serialize=False)),
                ('codigo_nacionalidad', models.SmallIntegerField(unique=True)),
                ('descripcion_nacionalidad', models.CharField(max_length=30)),
                ('estado_nacionalidad', models.BooleanField()),
                ('insercion_nacionalidad', models.DateTimeField()),
                ('modificacion_nacionalidad', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'nacionalidad',
            },
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id_persona', models.BigAutoField(primary_key=True, serialize=False)),
                ('codigo_persona', models.BigIntegerField(unique=True)),
                ('nombres_persona', models.CharField(max_length=30)),
                ('apellidos_persona', models.CharField(max_length=30)),
                ('nacimiento_persona', models.DateTimeField()),
                ('direccion_persona', models.CharField(max_length=50)),
                ('activo_persona', models.BooleanField()),
                ('insercion_persona', models.DateTimeField()),
                ('modificacion_persona', models.DateTimeField(blank=True, null=True)),
                ('id_barrio_fk_persona', models.ForeignKey(db_column='id_barrio_fk_persona', on_delete=django.db.models.deletion.CASCADE, to='app.barrio')),
                ('id_nacionalidad_fk_persona', models.ForeignKey(db_column='id_nacionalidad_fk_persona', on_delete=django.db.models.deletion.CASCADE, to='app.nacionalidad')),
            ],
            options={
                'db_table': 'persona',
            },
        ),
        migrations.CreateModel(
            name='Prioridad',
            fields=[
                ('id_prioridad', models.SmallAutoField(primary_key=True, serialize=False)),
                ('codigo_prioridad', models.SmallIntegerField(unique=True)),
                ('descripcion_prioridad', models.CharField(max_length=25)),
                ('estado_prioridad', models.BooleanField()),
                ('insercion_prioridad', models.DateTimeField()),
                ('modificacion_prioridad', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'prioridad',
            },
        ),
        migrations.CreateModel(
            name='PuestoAtencion',
            fields=[
                ('id_puesto_atencion', models.AutoField(primary_key=True, serialize=False)),
                ('nro_puesto_atencion', models.SmallIntegerField(unique=True)),
                ('estado_puesto_atencion', models.BooleanField()),
                ('insercion_puesto_atencion', models.DateTimeField()),
                ('modificacion_puesto_atencion', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'puesto_atencion',
            },
        ),
        migrations.CreateModel(
            name='Sexo',
            fields=[
                ('id_sexo', models.SmallAutoField(primary_key=True, serialize=False)),
                ('codigo_sexo', models.SmallIntegerField(unique=True)),
                ('descripcion_sexo', models.CharField(max_length=25)),
                ('estado_sexo', models.BooleanField()),
                ('insercion_sexo', models.DateTimeField()),
                ('modificacion_sexo', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'sexo',
            },
        ),
        migrations.CreateModel(
            name='TipoCliente',
            fields=[
                ('id_tipo_cliente', models.SmallAutoField(primary_key=True, serialize=False)),
                ('codigo_tipo_cliente', models.SmallIntegerField(unique=True)),
                ('descripcion_tipo_cliente', models.CharField(max_length=30)),
                ('estado_tipo_cliente', models.BooleanField()),
                ('insercion_tipo_cliente', models.DateTimeField()),
                ('modificacion_tipo_cliente', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'tipo_cliente',
            },
        ),
        migrations.CreateModel(
            name='TipoContacto',
            fields=[
                ('id_tipo_contacto', models.SmallAutoField(primary_key=True, serialize=False)),
                ('codigo_tipo_contacto', models.SmallIntegerField(unique=True)),
                ('descripcion_tipo_contacto', models.CharField(max_length=30)),
                ('estado_tipo_contacto', models.BooleanField()),
                ('insercion_tipo_contacto', models.DateTimeField()),
                ('modificacion_tipo_contacto', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'tipo_contacto',
            },
        ),
        migrations.CreateModel(
            name='TipoDocumento',
            fields=[
                ('id_tipo_documento', models.SmallAutoField(primary_key=True, serialize=False)),
                ('codigo_tipo_documento', models.SmallIntegerField(unique=True)),
                ('descripcion_tipo_documento', models.CharField(max_length=30)),
                ('estado_tipo_documento', models.BooleanField()),
                ('insercion_tipo_documento', models.DateTimeField()),
                ('modificacion_tipo_documento', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'tipo_documento',
            },
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id_usuario', models.BigAutoField(primary_key=True, serialize=False)),
                ('codigo_usuario', models.BigIntegerField(unique=True)),
                ('nombre_usuario', models.CharField(max_length=16)),
                ('clave_usuario', models.CharField(max_length=32)),
                ('activo_usuario', models.BooleanField()),
                ('insercion_usuario', models.DateTimeField()),
                ('modificacion_usuario', models.DateTimeField(blank=True, null=True)),
                ('id_persona_fk_usuario', models.ForeignKey(db_column='id_persona_fk_usuario', on_delete=django.db.models.deletion.CASCADE, to='app.persona')),
            ],
            options={
                'db_table': 'usuario',
            },
        ),
        migrations.CreateModel(
            name='Sucursal',
            fields=[
                ('id_sucursal', models.AutoField(primary_key=True, serialize=False)),
                ('nro_sucursal', models.SmallIntegerField(unique=True)),
                ('descripcion_sucursal', models.CharField(max_length=30)),
                ('direccion_sucursal', models.CharField(max_length=50)),
                ('referencia_sucursal', models.CharField(blank=True, max_length=32, null=True)),
                ('estado_sucursal', models.BooleanField()),
                ('insercion_sucursal', models.DateTimeField()),
                ('modificacion_sucursal', models.DateTimeField(blank=True, null=True)),
                ('id_barrio_fk_sucursal', models.ForeignKey(db_column='id_barrio_fk_sucursal', on_delete=django.db.models.deletion.CASCADE, to='app.barrio')),
            ],
            options={
                'db_table': 'sucursal',
            },
        ),
        migrations.CreateModel(
            name='Servicio',
            fields=[
                ('id_servicio', models.BigAutoField(primary_key=True, serialize=False)),
                ('codigo_servicio', models.BigIntegerField(unique=True)),
                ('descripcion_servicio', models.CharField(max_length=32)),
                ('costo_servicio', models.IntegerField()),
                ('estado_servicio', models.BooleanField()),
                ('insercion_servicio', models.DateTimeField()),
                ('modificacion_servicio', models.DateTimeField(blank=True, null=True)),
                ('id_categoria_servicio_fk_servicio', models.ForeignKey(db_column='id_categoria_servicio_fk_servicio', on_delete=django.db.models.deletion.CASCADE, to='app.categoriaservicio')),
                ('id_detalles_estado_servicio_fk_servicio', models.ForeignKey(db_column='id_detalles_estado_servicio_fk_servicio', on_delete=django.db.models.deletion.CASCADE, to='app.detallesestadoservicio')),
            ],
            options={
                'db_table': 'servicio',
            },
        ),
        migrations.CreateModel(
            name='Rol',
            fields=[
                ('id_rol', models.AutoField(primary_key=True, serialize=False)),
                ('codigo_rol', models.CharField(max_length=8, unique=True)),
                ('descripcion_rol', models.CharField(max_length=25)),
                ('activo_rol', models.BooleanField()),
                ('insercion_rol', models.DateTimeField()),
                ('modificacion_rol', models.DateTimeField(blank=True, null=True)),
                ('id_usuario_fk_rol', models.ForeignKey(db_column='id_usuario_fk_rol', on_delete=django.db.models.deletion.CASCADE, to='app.usuario')),
            ],
            options={
                'db_table': 'rol',
            },
        ),
        migrations.CreateModel(
            name='Reserva',
            fields=[
                ('id_reserva', models.BigAutoField(primary_key=True, serialize=False)),
                ('codigo_reserva', models.BigIntegerField(unique=True)),
                ('fecha_reserva', models.DateTimeField()),
                ('inicio_reserva', models.DateTimeField()),
                ('fin_reserva', models.DateTimeField(blank=True, null=True)),
                ('estado_reserva', models.CharField(max_length=16)),
                ('insercion_reserva', models.DateTimeField()),
                ('modificacion_reserva', models.DateTimeField(blank=True, null=True)),
                ('id_cliente_fk_reserva', models.ForeignKey(db_column='id_cliente_fk_reserva', on_delete=django.db.models.deletion.CASCADE, to='app.cliente')),
                ('id_puesto_atencion_fk_reserva', models.ForeignKey(db_column='id_puesto_atencion_fk_reserva', on_delete=django.db.models.deletion.CASCADE, to='app.puestoatencion')),
                ('id_servicio_fk_reserva', models.ForeignKey(db_column='id_servicio_fk_reserva', on_delete=django.db.models.deletion.CASCADE, to='app.servicio')),
                ('id_sucursal_fk_reserva', models.ForeignKey(db_column='id_sucursal_fk_reserva', on_delete=django.db.models.deletion.CASCADE, to='app.sucursal')),
            ],
            options={
                'db_table': 'reserva',
            },
        ),
        migrations.AddField(
            model_name='puestoatencion',
            name='id_sucursal_puesto_atencion',
            field=models.ForeignKey(db_column='id_sucursal_puesto_atencion', on_delete=django.db.models.deletion.CASCADE, to='app.sucursal'),
        ),
        migrations.AddField(
            model_name='puestoatencion',
            name='id_usuario_puesto_atencion',
            field=models.ForeignKey(db_column='id_usuario_puesto_atencion', on_delete=django.db.models.deletion.CASCADE, to='app.usuario'),
        ),
        migrations.CreateModel(
            name='PrestadorServicio',
            fields=[
                ('id_prestador_servicio', models.BigAutoField(primary_key=True, serialize=False)),
                ('jornada_prestador_servicio', models.SmallIntegerField()),
                ('costo_jornada_prestador_servicio', models.IntegerField()),
                ('cargo_prestador_servicio', models.CharField(max_length=32)),
                ('id_persona_fk_prestador_servicio', models.ForeignKey(db_column='id_persona_fk_prestador_servicio', on_delete=django.db.models.deletion.CASCADE, to='app.persona')),
                ('id_sucursal_fk_prestador_servicio', models.ForeignKey(db_column='id_sucursal_fk_prestador_servicio', on_delete=django.db.models.deletion.CASCADE, to='app.sucursal')),
            ],
            options={
                'db_table': 'prestador_servicio',
            },
        ),
        migrations.AddField(
            model_name='persona',
            name='id_prioridad_fk_persona',
            field=models.ForeignKey(db_column='id_prioridad_fk_persona', on_delete=django.db.models.deletion.CASCADE, to='app.prioridad'),
        ),
        migrations.AddField(
            model_name='persona',
            name='id_sexo_fk_persona',
            field=models.ForeignKey(db_column='id_sexo_fk_persona', on_delete=django.db.models.deletion.CASCADE, to='app.sexo'),
        ),
        migrations.AddField(
            model_name='persona',
            name='id_tipo_contacto_fk_persona',
            field=models.ForeignKey(db_column='id_tipo_contacto_fk_persona', on_delete=django.db.models.deletion.CASCADE, to='app.tipocontacto'),
        ),
        migrations.AddField(
            model_name='persona',
            name='id_tipo_documento_fk_persona',
            field=models.ForeignKey(db_column='id_tipo_documento_fk_persona', on_delete=django.db.models.deletion.CASCADE, to='app.tipodocumento'),
        ),
        migrations.CreateModel(
            name='Permiso',
            fields=[
                ('id_permiso', models.IntegerField(primary_key=True, serialize=False)),
                ('codigo_permiso', models.IntegerField(unique=True)),
                ('descripcion_permiso', models.CharField(max_length=32)),
                ('activo_permiso', models.BooleanField()),
                ('insercion_permiso', models.DateTimeField()),
                ('modificacion_permiso', models.DateTimeField(blank=True, null=True)),
                ('id_rol_fk_permiso', models.ForeignKey(db_column='id_rol_fk_permiso', on_delete=django.db.models.deletion.CASCADE, to='app.rol')),
            ],
            options={
                'db_table': 'permiso',
            },
        ),
        migrations.CreateModel(
            name='Pais',
            fields=[
                ('id_pais', models.SmallAutoField(primary_key=True, serialize=False)),
                ('codigo_iso2', models.CharField(max_length=2)),
                ('codigo_iso3', models.CharField(max_length=3)),
                ('descripcion_pais', models.CharField(max_length=30)),
                ('estado_pais', models.BooleanField()),
                ('insercion_pais', models.DateTimeField()),
                ('modificacion_pais', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'pais',
                'unique_together': {('codigo_iso2', 'codigo_iso3')},
            },
        ),
        migrations.AddField(
            model_name='nacionalidad',
            name='id_pais_fk_nacionalidad',
            field=models.ForeignKey(db_column='id_pais_fk_nacionalidad', on_delete=django.db.models.deletion.CASCADE, to='app.pais'),
        ),
        migrations.CreateModel(
            name='Dia',
            fields=[
                ('id_dia', models.SmallAutoField(primary_key=True, serialize=False)),
                ('descripcion_dia', models.CharField(max_length=16)),
                ('id_horario_fk_dia', models.ForeignKey(db_column='id_horario_fk_dia', on_delete=django.db.models.deletion.CASCADE, to='app.horario')),
                ('id_sucursal_fk_dia', models.ForeignKey(db_column='id_sucursal_fk_dia', on_delete=django.db.models.deletion.CASCADE, to='app.sucursal')),
            ],
            options={
                'db_table': 'dia',
            },
        ),
        migrations.AddField(
            model_name='detallesestadoservicio',
            name='id_prestador_servicio_fk_detalles_estado_servicios',
            field=models.ForeignKey(db_column='id_prestador_servicio_fk_detalles_estado_servicios', on_delete=django.db.models.deletion.CASCADE, to='app.prestadorservicio'),
        ),
        migrations.CreateModel(
            name='DetallesDocumento',
            fields=[
                ('id_detalles_documento', models.SmallAutoField(primary_key=True, serialize=False)),
                ('nro_documento', models.CharField(max_length=30, unique=True)),
                ('estado_detalles_documento', models.CharField(blank=True, max_length=30, null=True)),
                ('valido_desde', models.DateTimeField()),
                ('valido_hasta', models.DateTimeField()),
                ('modificacion_detalles_documento', models.DateTimeField(blank=True, null=True)),
                ('id_persona_fk_detalles_documento', models.ForeignKey(db_column='id_persona_fk_detalles_documento', on_delete=django.db.models.deletion.CASCADE, to='app.persona')),
            ],
            options={
                'db_table': 'detalles_documento',
            },
        ),
        migrations.CreateModel(
            name='DetallesContacto',
            fields=[
                ('id_detalles_contacto', models.SmallAutoField(primary_key=True, serialize=False)),
                ('codigo_detalle_contacto', models.SmallIntegerField(unique=True)),
                ('estado_detalle_contacto', models.BooleanField()),
                ('efectivo_detalle_contacto', models.CharField(max_length=50)),
                ('creacion_detalle_contacto', models.DateTimeField()),
                ('modifcacion_detalle_contacto', models.DateTimeField(blank=True, null=True)),
                ('id_persona_fk_detalles_contacto', models.ForeignKey(db_column='id_persona_fk_detalles_contacto', on_delete=django.db.models.deletion.CASCADE, to='app.persona')),
            ],
            options={
                'db_table': 'detalles_contacto',
            },
        ),
        migrations.CreateModel(
            name='Departamento',
            fields=[
                ('id_departamento', models.AutoField(primary_key=True, serialize=False)),
                ('codigo_departamento', models.SmallIntegerField(unique=True)),
                ('descripcion_departamento', models.CharField(max_length=30)),
                ('estado_departamento', models.BooleanField()),
                ('insercion_departamento', models.DateTimeField()),
                ('modificacion_departamento', models.DateTimeField(blank=True, null=True)),
                ('id_pais_fk_departamento', models.ForeignKey(db_column='id_pais_fk_departamento', on_delete=django.db.models.deletion.CASCADE, to='app.pais')),
            ],
            options={
                'db_table': 'departamento',
            },
        ),
        migrations.CreateModel(
            name='Condicion',
            fields=[
                ('id_condicion', models.SmallAutoField(primary_key=True, serialize=False)),
                ('codigo_condicion', models.SmallIntegerField(unique=True)),
                ('descripcion_condicion', models.CharField(max_length=30)),
                ('estado_condicion', models.BooleanField()),
                ('insercion_condicion', models.DateTimeField()),
                ('modificacion_condicion', models.DateTimeField(blank=True, null=True)),
                ('id_prioridad_fk_condicon', models.ForeignKey(db_column='id_prioridad_fk_condicon', on_delete=django.db.models.deletion.CASCADE, to='app.prioridad')),
            ],
            options={
                'db_table': 'condicion',
            },
        ),
        migrations.AddField(
            model_name='cliente',
            name='id_persona_fk_cliente',
            field=models.ForeignKey(db_column='id_persona_fk_cliente', on_delete=django.db.models.deletion.CASCADE, to='app.persona'),
        ),
        migrations.AddField(
            model_name='cliente',
            name='id_tipo_cliente_fk_cliente',
            field=models.ForeignKey(db_column='id_tipo_cliente_fk_cliente', on_delete=django.db.models.deletion.CASCADE, to='app.tipocliente'),
        ),
        migrations.CreateModel(
            name='Ciudad',
            fields=[
                ('id_ciudad', models.AutoField(primary_key=True, serialize=False)),
                ('codigo_ciudad', models.SmallIntegerField(unique=True)),
                ('descripcion_ciudad', models.CharField(max_length=30)),
                ('estado_ciudad', models.BooleanField()),
                ('insercion_ciudad', models.DateTimeField()),
                ('modificacion_ciudad', models.DateTimeField(blank=True, null=True)),
                ('id_departamento_fk_ciudad', models.ForeignKey(db_column='id_departamento_fk_ciudad', on_delete=django.db.models.deletion.CASCADE, to='app.departamento')),
            ],
            options={
                'db_table': 'ciudad',
            },
        ),
        migrations.AddField(
            model_name='barrio',
            name='id_ciudad_fk_barrio',
            field=models.ForeignKey(db_column='id_ciudad_fk_barrio', on_delete=django.db.models.deletion.CASCADE, to='app.ciudad'),
        ),
    ]
