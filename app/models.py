# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


#************************* TURNERO *************************#
class Pais(models.Model):
    id_pais = models.SmallAutoField(primary_key=True)
    codigo_iso2 = models.CharField(max_length=2)
    codigo_iso3 = models.CharField(max_length=3)
    descripcion_pais = models.CharField(max_length=30)
    estado_pais = models.BooleanField()
    insercion_pais = models.DateTimeField()
    modificacion_pais = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion_pais

    class Meta:
        db_table = 'pais'
        unique_together = (('codigo_iso2', 'codigo_iso3'),)


class Prioridad(models.Model):
    id_prioridad = models.SmallAutoField(primary_key=True)
    codigo_prioridad = models.SmallIntegerField(unique=True)
    descripcion_prioridad = models.CharField(max_length=25)
    estado_prioridad = models.BooleanField()
    insercion_prioridad = models.DateTimeField()
    modificacion_prioridad = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion_prioridad

    class Meta:
        db_table = 'prioridad'


class CategoriaServicio(models.Model):
    id_categoria_servicio = models.AutoField(primary_key=True)
    descripcion_categoria_servicio = models.CharField(max_length=32)
    estado_categoria_servicio = models.BooleanField()
    insercion_categoria_servicio = models.DateTimeField()
    modificacion_categoria_servicio = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion_categoria_servicio

    class Meta:
        db_table = 'categoria_servicio'


class Horario(models.Model):
    id_horario = models.AutoField(primary_key=True)
    inicio_horario = models.DateTimeField()
    fin_horario = models.DateTimeField()
    insercion_horario = models.DateTimeField()
    modificacion_horario = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'horario'


class Sexo(models.Model):
    id_sexo = models.SmallAutoField(primary_key=True)
    codigo_sexo = models.SmallIntegerField(unique=True)
    descripcion_sexo = models.CharField(max_length=25)
    estado_sexo = models.BooleanField()
    insercion_sexo = models.DateTimeField()
    modificacion_sexo = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion_sexo

    class Meta:
        db_table = 'sexo'


class TipoCliente(models.Model):
    id_tipo_cliente = models.SmallAutoField(primary_key=True)
    codigo_tipo_cliente = models.SmallIntegerField(unique=True)
    descripcion_tipo_cliente = models.CharField(max_length=30)
    estado_tipo_cliente = models.BooleanField()
    insercion_tipo_cliente = models.DateTimeField()
    modificacion_tipo_cliente = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'tipo_cliente'


class TipoContacto(models.Model):
    id_tipo_contacto = models.SmallAutoField(primary_key=True)
    codigo_tipo_contacto = models.SmallIntegerField(unique=True)
    descripcion_tipo_contacto = models.CharField(max_length=30)
    estado_tipo_contacto = models.BooleanField()
    insercion_tipo_contacto = models.DateTimeField()
    modificacion_tipo_contacto = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion_tipo_contacto

    class Meta:
        db_table = 'tipo_contacto'


class TipoDocumento(models.Model):
    id_tipo_documento = models.SmallAutoField(primary_key=True)
    codigo_tipo_documento = models.SmallIntegerField(unique=True)
    descripcion_tipo_documento = models.CharField(max_length=30)
    estado_tipo_documento = models.BooleanField()
    insercion_tipo_documento = models.DateTimeField()
    modificacion_tipo_documento = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion_tipo_documento

    class Meta:
        db_table = 'tipo_documento'


class Nacionalidad(models.Model):
    id_nacionalidad = models.SmallAutoField(primary_key=True)
    id_pais_fk_nacionalidad = models.ForeignKey('Pais', db_column='id_pais_fk_nacionalidad', on_delete=models.CASCADE)
    codigo_nacionalidad = models.SmallIntegerField(unique=True)
    descripcion_nacionalidad = models.CharField(max_length=30)
    estado_nacionalidad = models.BooleanField()
    insercion_nacionalidad = models.DateTimeField()
    modificacion_nacionalidad = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion_nacionalidad

    class Meta:
        db_table = 'nacionalidad'


class Departamento(models.Model):
    id_departamento = models.AutoField(primary_key=True)
    id_pais_fk_departamento = models.ForeignKey('Pais', db_column='id_pais_fk_departamento', on_delete=models.CASCADE)
    codigo_departamento = models.SmallIntegerField(unique=True)
    descripcion_departamento = models.CharField(max_length=30)
    estado_departamento = models.BooleanField()
    insercion_departamento = models.DateTimeField()
    modificacion_departamento = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion_departamento

    class Meta:
        db_table = 'departamento'


class Ciudad(models.Model):
    id_ciudad = models.AutoField(primary_key=True)
    id_departamento_fk_ciudad = models.ForeignKey('Departamento', db_column='id_departamento_fk_ciudad', on_delete=models.CASCADE)
    codigo_ciudad = models.SmallIntegerField(unique=True)
    descripcion_ciudad = models.CharField(max_length=30)
    estado_ciudad = models.BooleanField()
    insercion_ciudad = models.DateTimeField()
    modificacion_ciudad = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion_ciudad

    class Meta:
        db_table = 'ciudad'


class Barrio(models.Model):
    id_barrio = models.AutoField(primary_key=True)
    id_ciudad_fk_barrio = models.ForeignKey('Ciudad', db_column='id_ciudad_fk_barrio', on_delete=models.CASCADE)
    codigo_barrio = models.SmallIntegerField(unique=True)
    descripcion_barrio = models.CharField(max_length=30)
    estado_barrio = models.BooleanField()
    insercion_barrio = models.DateTimeField()
    modificacion_barrio = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.descripcion_barrio

    class Meta:
        db_table = 'barrio'


class Sucursal(models.Model):
    id_sucursal = models.AutoField(primary_key=True)
    id_barrio_fk_sucursal = models.ForeignKey('Barrio', db_column='id_barrio_fk_sucursal', on_delete=models.CASCADE)
    nro_sucursal = models.SmallIntegerField(unique=True)
    descripcion_sucursal = models.CharField(max_length=30)
    direccion_sucursal = models.CharField(max_length=50)
    referencia_sucursal = models.CharField(max_length=32, blank=True, null=True)
    estado_sucursal = models.BooleanField()
    insercion_sucursal = models.DateTimeField()
    modificacion_sucursal = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'sucursal'


class Condicion(models.Model):
    id_condicion = models.SmallAutoField(primary_key=True)
    id_prioridad_fk_condicon = models.ForeignKey('Prioridad', db_column='id_prioridad_fk_condicon', on_delete=models.CASCADE)
    codigo_condicion = models.SmallIntegerField(unique=True)
    descripcion_condicion = models.CharField(max_length=30)
    estado_condicion = models.BooleanField()
    insercion_condicion = models.DateTimeField()
    modificacion_condicion = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'condicion'


class Persona(models.Model):
    id_persona = models.BigAutoField(primary_key=True)
    # id_tipo_documento_fk_persona = models.ForeignKey('TipoDocumento', db_column='id_tipo_documento_fk_persona', on_delete=models.CASCADE)
    # id_tipo_contacto_fk_persona = models.ForeignKey('TipoContacto', db_column='id_tipo_contacto_fk_persona', on_delete=models.CASCADE)
    id_prioridad_fk_persona = models.ForeignKey('Prioridad', db_column='id_prioridad_fk_persona', on_delete=models.CASCADE)
    id_barrio_fk_persona = models.ForeignKey('Barrio', db_column='id_barrio_fk_persona', on_delete=models.CASCADE)
    id_sexo_fk_persona = models.ForeignKey('Sexo', db_column='id_sexo_fk_persona', on_delete=models.CASCADE)
    id_nacionalidad_fk_persona = models.ForeignKey('Nacionalidad', db_column='id_nacionalidad_fk_persona', on_delete=models.CASCADE)
    # codigo_persona = models.BigIntegerField(unique=True)
    nombres_persona = models.CharField(max_length=30)
    apellidos_persona = models.CharField(max_length=30)
    cedula_persona = models.TextField(max_length=10, null=True)
    nacimiento_persona = models.DateTimeField()
    direccion_persona = models.CharField(max_length=50)
    contacto_persona = models.TextField(max_length=100, null=True)
    correo_persona = models.EmailField(null=True)
    activo_persona = models.BooleanField()
    insercion_persona = models.DateTimeField()
    modificacion_persona = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.nombres_persona + ' ' + self.apellidos_persona

    class Meta:
        db_table = 'persona'


class Cliente(models.Model):
    id_cliente = models.BigAutoField(primary_key=True)
    id_persona_fk_cliente = models.ForeignKey('Persona', db_column='id_persona_fk_cliente', on_delete=models.CASCADE)
    id_tipo_cliente_fk_cliente = models.ForeignKey('TipoCliente', db_column='id_tipo_cliente_fk_cliente', on_delete=models.CASCADE)
    codigo_cliente = models.BigIntegerField(unique=True)
    razon_social = models.CharField(max_length=128)

    class Meta:
        db_table = 'cliente'


class Usuario(models.Model):
    id_usuario = models.BigAutoField(primary_key=True)
    id_persona_fk_usuario = models.ForeignKey('Persona', db_column='id_persona_fk_usuario', on_delete=models.CASCADE)
    codigo_usuario = models.BigIntegerField(unique=True)
    nombre_usuario = models.CharField(max_length=16)
    clave_usuario = models.CharField(max_length=32)
    activo_usuario = models.BooleanField()
    insercion_usuario = models.DateTimeField()
    modificacion_usuario = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'usuario'


class Rol(models.Model):
    id_rol = models.AutoField(primary_key=True)
    id_usuario_fk_rol = models.ForeignKey('Usuario', db_column='id_usuario_fk_rol', on_delete=models.CASCADE)
    codigo_rol = models.CharField(unique=True, max_length=8)
    descripcion_rol = models.CharField(max_length=25)
    activo_rol = models.BooleanField()
    insercion_rol = models.DateTimeField()
    modificacion_rol = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'rol'


class Permiso(models.Model):
    id_permiso = models.IntegerField(primary_key=True)
    id_rol_fk_permiso = models.ForeignKey('Rol', db_column='id_rol_fk_permiso', on_delete=models.CASCADE)
    codigo_permiso = models.IntegerField(unique=True)
    descripcion_permiso = models.CharField(max_length=32)
    activo_permiso = models.BooleanField()
    insercion_permiso = models.DateTimeField()
    modificacion_permiso = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'permiso'


class DetallesContacto(models.Model):
    id_detalles_contacto = models.SmallAutoField(primary_key=True)
    id_persona_fk_detalles_contacto = models.ForeignKey('Persona', db_column='id_persona_fk_detalles_contacto', on_delete=models.CASCADE)
    codigo_detalle_contacto = models.SmallIntegerField(unique=True)
    estado_detalle_contacto = models.BooleanField()
    efectivo_detalle_contacto = models.CharField(max_length=50)
    creacion_detalle_contacto = models.DateTimeField()
    modifcacion_detalle_contacto = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'detalles_contacto'


class DetallesDocumento(models.Model):
    id_detalles_documento = models.SmallAutoField(primary_key=True)
    id_persona_fk_detalles_documento = models.ForeignKey('Persona', db_column='id_persona_fk_detalles_documento', on_delete=models.CASCADE)
    nro_documento = models.CharField(unique=True, max_length=30)
    estado_detalles_documento = models.CharField(max_length=30, blank=True, null=True)
    valido_desde = models.DateTimeField()
    valido_hasta = models.DateTimeField()
    modificacion_detalles_documento = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'detalles_documento'


class PrestadorServicio(models.Model):
    id_prestador_servicio = models.BigAutoField(primary_key=True)
    id_persona_fk_prestador_servicio = models.ForeignKey('Persona', db_column='id_persona_fk_prestador_servicio', on_delete=models.CASCADE)
    id_sucursal_fk_prestador_servicio = models.ForeignKey('Sucursal', db_column='id_sucursal_fk_prestador_servicio', on_delete=models.CASCADE)
    jornada_prestador_servicio = models.SmallIntegerField()
    costo_jornada_prestador_servicio = models.IntegerField()
    cargo_prestador_servicio = models.CharField(max_length=32)

    class Meta:
        db_table = 'prestador_servicio'


class DetallesEstadoServicio(models.Model):
    id_detalles_estado_servicio = models.BigAutoField(primary_key=True)
    id_prestador_servicio_fk_detalles_estado_servicios = models.ForeignKey('PrestadorServicio', db_column='id_prestador_servicio_fk_detalles_estado_servicios', on_delete=models.CASCADE)
    turnos_diarios = models.IntegerField()
    turnos_restantes = models.IntegerField()
    estimacion_tiempo_turno = models.IntegerField()

    class Meta:
        db_table = 'detalles_estado_servicio'


class Servicio(models.Model):
    id_servicio = models.BigAutoField(primary_key=True)
    # id_detalles_estado_servicio_fk_servicio = models.ForeignKey('DetallesEstadoServicio', db_column='id_detalles_estado_servicio_fk_servicio', on_delete=models.CASCADE)
    id_categoria_servicio_fk_servicio = models.ForeignKey('CategoriaServicio', db_column='id_categoria_servicio_fk_servicio', on_delete=models.CASCADE)
    # codigo_servicio = models.BigIntegerField(unique=True)
    descripcion_servicio = models.CharField(max_length=100)
    costo_servicio = models.IntegerField()
    estado_servicio = models.BooleanField()
    insercion_servicio = models.DateTimeField()
    modificacion_servicio = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return f"{self.descripcion_servicio} ({self.id_categoria_servicio_fk_servicio})"

    class Meta:
        db_table = 'servicio'


class PuestoAtencion(models.Model):
    id_puesto_atencion = models.AutoField(primary_key=True)
    id_sucursal_puesto_atencion = models.ForeignKey('Sucursal', db_column='id_sucursal_puesto_atencion', on_delete=models.CASCADE)
    id_usuario_puesto_atencion = models.ForeignKey('Usuario', db_column='id_usuario_puesto_atencion', on_delete=models.CASCADE)
    nro_puesto_atencion = models.SmallIntegerField(unique=True)
    estado_puesto_atencion = models.BooleanField()
    insercion_puesto_atencion = models.DateTimeField()
    modificacion_puesto_atencion = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'puesto_atencion'


class Reserva(models.Model):
    id_reserva = models.BigAutoField(primary_key=True)
    id_puesto_atencion_fk_reserva = models.ForeignKey('PuestoAtencion', db_column='id_puesto_atencion_fk_reserva', on_delete=models.CASCADE)
    id_cliente_fk_reserva = models.ForeignKey('Cliente', db_column='id_cliente_fk_reserva', on_delete=models.CASCADE)
    id_servicio_fk_reserva = models.ForeignKey('Servicio', db_column='id_servicio_fk_reserva', on_delete=models.CASCADE)
    id_sucursal_fk_reserva = models.ForeignKey('Sucursal', db_column='id_sucursal_fk_reserva', on_delete=models.CASCADE)
    codigo_reserva = models.BigIntegerField(unique=True)
    fecha_reserva = models.DateTimeField()
    inicio_reserva = models.DateTimeField()
    fin_reserva = models.DateTimeField(blank=True, null=True)
    estado_reserva = models.CharField(max_length=16)
    insercion_reserva = models.DateTimeField()
    modificacion_reserva = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'reserva'


class Dia(models.Model):
    id_dia = models.SmallAutoField(primary_key=True)
    id_sucursal_fk_dia = models.ForeignKey('Sucursal', db_column='id_sucursal_fk_dia', on_delete=models.CASCADE)
    id_horario_fk_dia = models.ForeignKey('Horario', db_column='id_horario_fk_dia', on_delete=models.CASCADE)
    descripcion_dia = models.CharField(max_length=16)

    class Meta:
        db_table = 'dia'        
