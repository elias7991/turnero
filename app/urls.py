from django.urls import path
from .views import index, settings, AddUser, AddService, ListUsers, ListService, DeleteUser, DeleteService


urlpatterns = [
  path("", index, name="index"),
  path("servicios", ListService.as_view(), name="services"),
  path("usuarios", ListUsers.as_view(), name="users"),
  path("configuraciones", settings, name="settings"),
  path("agregar_usuario", AddUser.as_view(), name="add_user"),
  path("agregar_servicio", AddService.as_view(), name="add_service"),
  path("eliminar_usuario/<int:pk>", DeleteUser.as_view(), name="delete_user"),
  path("eliminar_servicio/<int:pk>", DeleteService.as_view(), name="delete_service")

]