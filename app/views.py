from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView

from .forms import AddPersonForm, AddServiceForm
from .models import Persona, Servicio


# Create your views here.
def index(request):
  return render(request, "index.html")


def settings(request):
  return render(request, "settings.html")

class ListUsers(ListView):
  model = Persona
  template_name = "users.html"

class ListService(ListView):
  model = Servicio
  template_name = "services.html"

class AddUser(CreateView):
  model = Persona
  form_class = AddPersonForm
  template_name = "add_person.html"
  success_url = reverse_lazy("users")

class AddService(CreateView):
  model = Servicio
  form_class = AddServiceForm
  template_name = "add_service.html"
  success_url = reverse_lazy("services")
  
class DeleteUser(DeleteView):
  model = Persona
  success_url = reverse_lazy("users")
  template_name = "persona_confirm_delete.html"

class DeleteService(DeleteView):
  model = Servicio
  success_url = reverse_lazy("services")
  template_name = "servicio_confirm_delete.html"